(ns gitlog-obfuscator.core-test
  (:require [clojure.test :refer :all]
            [gitlog-obfuscator.core :refer :all]
            [clojure.string :as str]))

(deftest log-line-parsing
  (testing "handles empty lines"
    (is (= [:empty-line]
           (second (parse-log-line "\n")))))
  (testing "parses a commit hash line"
    (is (= [:commit-hash "31548e8d8f8b54e079a9e9bc4567938b842776f2"]
           (second (parse-log-line "commit 31548e8d8f8b54e079a9e9bc4567938b842776f2")))))
  (testing "parses an author header line"
    (is (= [:header "Author" "Axel Schüssler <axel@axels.cloud>"]
           (second (parse-log-line "Author: Axel Schüssler <axel@axels.cloud>")))))
  (testing "parses commit message line"
    (is (= [:message "Initial commit"]
           (second (parse-log-line "    Initial commit")))))
  (testing "parses empty message line"
    (is (= [:message ""]
           (second (parse-log-line "    ")))))
  (testing "parses file stats line"
    (is (= [:file-stats "4" "1" "my/test/file.txt"]
           (second (parse-log-line "4\t1\tmy/test/file.txt")))))
  (testing "parses file change line with file modification"
    (is (= [:file-modification "A" "my/test/file.txt"]
           (second (parse-log-line "A\tmy/test/file.txt")))))
  (testing "parses file change line with file relocation"
    (is (= [:file-relocation "R" "099" "my/test/A.txt" "my/test/B.txt"]
           (second (parse-log-line "R099\tmy/test/A.txt\tmy/test/B.txt")))))
  (testing "handles parse errors"
    (is (= ["ERROR: Some meaningless line." [:unknown]]
           (parse-log-line "Some meaningless line.")))))

(deftest log-line-obfuscation
  (testing "leaves a commit hash line unchanged"
    (is (= "commit 31548e8d8f8b54e079a9e9bc4567938b842776f2"
           (obfuscate {} (parse-log-line "commit 31548e8d8f8b54e079a9e9bc4567938b842776f2")))))
  (testing "replaces author in header"
    (is (= "Author: Developer A"
           (obfuscate {:devs {"Axel Schüssler <axel@axels.cloud>" "Developer A"}}
                      (parse-log-line "Author: Axel Schüssler <axel@axels.cloud>")))))
  (testing "generates error on unmapped developer"
    (is (= "Author: ERROR: Unmapped developer!"
           (obfuscate {} (parse-log-line "Author: Axel Schüssler <axel@axels.cloud>")))))
  (testing "obfuscates file in file-stats line"
    (is (= "4\t1\t535ce39c/ecd71870/b2a34a06.txt"
           (obfuscate {:salt 123}
                      (parse-log-line "4\t1\tmy/test/file.txt")))))
  (testing "does not obfuscate specific path segments"
    (is (= "5\t2\td53c840a/src/main/38a0963a/3b9c358f.txt"
           (obfuscate {:ignored-path-segments #{"src" "main"}}
                      (parse-log-line "5\t2\tsomeproject/src/main/java/file.txt")))))
  (testing "obfuscates file in file-modification line"
    (is (= "A\tb2a34a06.txt"
           (obfuscate {:salt 123}
                      (parse-log-line "A\tfile.txt")))))
  (testing "obfuscates file in file-relocation line"
    (is (= "R099\t535ce39c/ecd71870/44b6e7cc.txt\t535ce39c/ecd71870/92bc214a.txt"
           (obfuscate {:salt 123}
                      (parse-log-line "R099\tmy/test/A.txt\tmy/test/B.txt")))))
  (testing "obfuscates non-empty message line"
    (is (= "    Message line redacted."
           (obfuscate {} (parse-log-line "    Some commit message.")))))
  (testing "ignores empty message line"
    (is (= "    "
           (obfuscate {} (parse-log-line "    ")))))
  (testing "ignores message line that indicates a reversed commit"
    (is (= "    This reverts commit f00d44416bde3a3615ec898de76564acc3c614d9."
           (obfuscate {:leave-revert-messages true}
                      (parse-log-line "    This reverts commit f00d44416bde3a3615ec898de76564acc3c614d9."))))))

(deftest config-file-creation
  (testing "extracts developer from Author header"
    (is (= {:salt 123
            :devs {"Axel Schüssler <axel@axels.cloud>"
                   "dev-af994608"}}
           (extract {:salt 123}
                    (parse-log-line "Author: Axel Schüssler <axel@axels.cloud>")))))
  (testing "extracts developer from Commit header"
    (is (= {:salt 123
            :devs {"Axel Schüssler <axel@axels.cloud>"
                   "dev-af994608"}}
           (extract {:salt 123}
                    (parse-log-line "Commit: Axel Schüssler <axel@axels.cloud>"))))))
