# gitlog-obfuscator

A configurable two-pass obfuscator for git commit logs.

It replaces potentially sensitive information (developer names and email
addresses, commit messages, and filenames) with hashes.  It currently works on
commit logs with line statistics and file status information:

    $ git log --numstat --no-renames > repository.txt \
      && git log --name-status >> repository.txt


## Usage

To run the obfuscator, we will use [Leiningen][1].

In a first pass, we extract authors and committers into a configuration file.
The following command will create `repository.edn`, if it does not exist.

    $ lein run repository.txt repository.edn

Edit the configuration file to customize the obfuscation:

```clojure
{:salt 123,
 :ignored-path-segments #{"src" "main" "test"},
 :devs {"Axel Schüssler <axel@axels.cloud>" "dev-af994608"}}
```

The `:devs` map contains developer names and addresses mapped to an
automatically generated pseudonym.  You can adapt the pseudonyms, but all
extracted names and addresses must be present in the map and have a pseudonym
assigned, or they won't be obfuscated.  `:ignored-path-segments` can be
extended by folder names that can appear in plain text.  If you want to keep
message lines indicating a reverted commit, set `:leave-revert-messages` to
`true`.

The `:salt` was randomly generated and will be used for all hashes. You can
leave it as it is.

Afterwards, execute the same command with the configuration file present:

    $ lein run repository.txt repository.edn

This will create the obfuscated file `repository.txt.obfuscated`.


[1]: https://github.com/technomancy/leiningen


## Result

A plain commit log entry looks like this:

```
commit 31548e8d8f8b54e079a9e9bc4567938b842776f2
Author: Axel Schüssler <axel@axels.cloud>
Date:   Fri Mar 20 14:30:57 2020 +0100

    PRJ-123: Change to my application

209	0	project/src/main/java/cloud/axels/enterprisey/project/Application.java
```

The obfuscated version:

```
commit 31548e8d8f8b54e079a9e9bc4567938b842776f2
Author: dev-a1fe86b1
Date:   Fri Mar 20 14:30:57 2020 +0100

    Message line redacted.

209	0	b0352fab/src/main/java/9b976b71/6eaaa99b/7e3a1b8a/583c6506/f5788b6d.java
```


## License

Copyright © 2020 Axel Schüssler

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.

