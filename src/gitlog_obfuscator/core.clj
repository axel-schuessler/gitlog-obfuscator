(ns gitlog-obfuscator.core
  (:gen-class)
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :as pp]
            [clojure.string :as str]
            [instaparse.core :as insta])
  (:import java.security.MessageDigest))

(def parse-line (insta/parser (io/resource "gitlog-line.bnf")))

(defn error [s] (str "ERROR: " s))

(defn parse-log-line [line]
  (let [parsed (parse-line line)]
    (if (insta/failure? parsed)
      [(error line) [:unknown]]
      [line (second parsed)])))

(def dev-headers #{"Author" "Commit"})

(defn- sha256 [s]
  (let [digest (.digest (MessageDigest/getInstance "SHA-256")
                        (.getBytes s "UTF-8"))]
    (apply str (map (partial format "%02x") digest))))

(defn- salted-hash
  "The first eight bytes of the salted hash of the specified string."
  [s salt]
  (subs (sha256 (str s salt)) 0 8))

(defn- extract-dev [cfg [type name value]]
  (if (and (= :header type) (dev-headers name))
    (assoc-in cfg [:devs value]
              (str "dev-" (salted-hash value (:salt cfg))))
    cfg))

(defn extract [m [_ parsed]]
  (extract-dev m parsed))

(def default-config {:salt (rand-int Integer/MAX_VALUE)
                     :ignored-path-segments #{"src" "main" "test"}
                     :leave-revert-messages true
                     :devs {}})

(defn- create-config-file [source-file config-file]
  (with-open [r (io/reader source-file)
              w (io/writer config-file)]
    (pp/pprint (->> (line-seq r)
                    (map parse-log-line)
                    (reduce extract default-config)) w)))

(defn- obfuscate-dev
  "A header line with a developers name and email address replaced with
  the pseudonym specified in the configuration file."
  [cfg line [_ name value]]
  (if (dev-headers name)
    (str/replace line value
                 (get-in cfg [:devs value]
                         (error "Unmapped developer!")))
    line))

(defn- obfuscate-filename
  "Obfuscates a filename by hashing path segments and filename,
  while preserving specific path segments and the file extension."
  [cfg filename line]
  (let [path-segments (str/split filename #"/")
        hash (fn [s] (salted-hash s (:salt cfg)))
        ignored-path-segment? (get cfg :ignored-path-segments #{})
        path (->> path-segments
                  (butlast)
                  (map #(if (ignored-path-segment? %) % (hash %)))
                  (str/join "/"))
        name (let [[name ext] (str/split (last path-segments) #"(?=\.[a-zA-Z0-9]+$)")]
               (str (hash name) ext))]
    (str/replace line filename (if (not-empty path)
                                 (str path "/" name)
                                 name))))

(defn- obfuscate-message [cfg parsed line]
  (if (or (empty? (second parsed))
          (and (:leave-revert-messages cfg)
               (re-matches #"    This reverts commit [0-9a-f]+." line)))
    line
    "    Message line redacted."))

(defn obfuscate [cfg [line parsed]]
  (case (first parsed)
    :header (obfuscate-dev cfg line parsed)
    :message (obfuscate-message cfg parsed line)
    :file-stats (obfuscate-filename cfg (last parsed) line)
    :file-modification (obfuscate-filename cfg (last parsed) line)
    :file-relocation (->> line
                          (obfuscate-filename cfg (last (butlast parsed)))
                          (obfuscate-filename cfg (last parsed)))
    line))

(defn obfuscate-file [source-file config-file obfuscated-file]
  (with-open [r (io/reader source-file)
              w (io/writer obfuscated-file)]
    (let [cfg (edn/read-string (slurp config-file))]
      (->> (line-seq r)
           (map parse-log-line)
           (map (partial obfuscate cfg))
           (run! #(do (.write w %) (.newLine w)))))))

(defn -main [source-file config-file]
  (if (.exists (io/file config-file))
    (let [obfuscated-file (str source-file ".obfuscated")]
      (do (println (format "Second pass: Obfuscating file: %s -> %s ..." source-file obfuscated-file))
          (obfuscate-file source-file config-file obfuscated-file)
          (println "Done.")))
    (do (println "First pass: Creating configuration file ...")
        (create-config-file source-file config-file)
        (println (format "Done. Please check '%s', then run again!" config-file)))))
